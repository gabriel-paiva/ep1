#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>

using namespace std;

class player{
// Atributos
private:
	string nome;
	int quant_barcos;
public:
	player();
	player(string nome, int quant_barcos);
	~player();
	//Métodos acessores:
	void set_nome(string nome);
	string get_nome();
	void set_quant_barcos(int quant_barcos);
	int get_quant_barcos();


};

#endif