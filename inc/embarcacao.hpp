#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <string>

using namespace std;

class Embarcacao {
//Atributos
	private:
		string nome, direcao;
		int vida, linha, coluna, contador[2];

//Metodos
	public:
		Embarcacao();
		Embarcacao(string nome, int vida, int linha, int coluna, string direcao);
		~Embarcacao();
		//Métodos acessores
		void set_nome (string nome);
		string get_nome();
		void set_vida (int vida);
		int get_vida();
		void set_linha (int linha);
		int get_linha();
		void set_coluna (int coluna);
		int get_coluna();
		void set_direcao (string direcao);
		string get_direcao();

		void set_contador(int pos, int conteudo);
		int get_contador(int pos);

};

#endif
