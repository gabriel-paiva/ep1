#ifndef MAPA_HPP
#define MAPA_HPP

#include <string>
#include <vector>
#include "../inc/embarcacao.hpp"
#include "../inc/portaavioes.hpp"
#include "../inc/submarino.hpp"
#include "../inc/canoa.hpp"

#define TAM_MAPA 13

using namespace std;

class mapa{
//Atributos:
private:
	string interface[TAM_MAPA][TAM_MAPA];
	int enderecos[TAM_MAPA][TAM_MAPA];

//Métodos
public:
	//Struct:
	struct filebarcos{
		string nome, direcao;
		int linha, coluna;
	};

	mapa();
	~mapa();

	void set_interface(int linha, int coluna, string conteudo);
	string get_interface(int linha, int coluna);
	void printa_interface();

	void set_enderecos(int linha, int coluna, int conteudo, string nome, string direcao);
	void set_endereco(int linha, int coluna, int conteudo);
	int get_enderecos(int linha, int coluna);

	int checa_ataque(int linha, int coluna);
	int gera_aleatorio();

};

#endif