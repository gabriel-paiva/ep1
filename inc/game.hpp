#ifndef GAME_HPP
#define GAME_HPP

#include <fstream>
#include <string>
#include <vector>

using namespace std;

class game{
	private:
	public:
	game();
	~game();
	void carrega_struct();
	void ingame();
	int converte_str(string str);
	vector<string> le_arquivo(string namefile);
};


#endif