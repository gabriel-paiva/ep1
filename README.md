# EP1 - OO 2019.1 (UnB - Gama) - Aluno: Gabriel Paiva Aguiar

Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

O código escrito pelo aluno Gabriel Paiva Aguiar (Matricula: 180016938), para ser submetido à avaliação, se encontra também no gitlab, no link: https://gitlab.com/gabriel-paiva/ep1

## Instruções

* Descrição do projeto
	Ao executar o projeto (instruções abaixo), deve-se escolher (com números) a opção para começar o jogo ou fechar o programa. Iniciando o jogo, deve-se inserir os nomes dos jogadores (sem espaços) e escolher o mapa desejado (dentre as opções estão somente os mapas fornecidos pelos monitores da disciplina).
	Os ataques são feitos em turnos, começando pelo Jogador 1. Usa-se somente números de 0 a 12, separados por espaço, para indicar a posição do tabuleiro que será atacada. Após cada ataque, deve-se esperar 2 segundos para sua resolução. Nesse tempo, haverá um feedback na tela da posição que foi atacada. Após esse tempo, o próximo jogador pode entrar em cena.
	Caso deseje abortar o jogo antes de seu fim, de modo seguro, digite "13" em qualquer lacuna do ataque (linha ou coluna), para que ele seja encerrado de modo que a memória seja desalocada de maneira correta.
	O jogo terá fim quando qualquer um dos dois jogadores fique sem barcos. Após isso, é mostrada a pontuação de seu rival e é dada a opção de jogar novamente.

* Instruções de execução

De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações
