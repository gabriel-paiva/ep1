#include "../inc/mapa.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#define TAM_MAPA 13

//Struct:
	struct filebarcos{
		string nome, direcao;
		int linha, coluna;
	};

//Métodos construtores:
mapa::mapa(){
	for(int linha =0;linha<TAM_MAPA;linha++){
			for(int coluna=0;coluna<TAM_MAPA;coluna++){
				enderecos[linha][coluna] = 300;
			}
		}

		for(int linha =0;linha<TAM_MAPA;linha++){
			for(int coluna=0;coluna<TAM_MAPA;coluna++){
				interface[linha][coluna] = "[ ]";
			}
		}
}
mapa::~mapa(){}

//Métodos acessores:
	void mapa::set_interface(int linha, int coluna, string conteudo){
		interface[linha][coluna] = conteudo;
	}
	string mapa::get_interface(int linha, int coluna){
		return interface[linha][coluna];
	}
	void mapa::printa_interface(){
		int q= 0;
		cout << setw(43) << setfill('-')<<'\n';
		for(q=0;q<TAM_MAPA;q++){
			cout << setw(2) << "--" << q;
		}

		cout << endl;
		cout << setw(43) << setfill('-')<<'\n';



		for(int linha =0;linha<TAM_MAPA;linha++){

			cout << setw(1) << setfill('-') << setiosflags(ios::right) << linha;
			if(linha > 9){
				cout << "|";
			}
			else{
				cout << " |";
			}

			for(int coluna=0;coluna<TAM_MAPA;coluna++){
				cout << interface[linha][coluna];
				if (coluna == 12){
					cout << endl;
				}
			}
			//cout << setw(43) << setfill('-')<<'\n';
		}
	}

	void mapa::set_enderecos(int linha, int coluna, int conteudo, string nome, string direcao){
		int tamanho;
		// Setar o tamanho de cada embarcação
		if(nome[0] == 's'){
			tamanho = 2;
		}
		else if(nome[0] =='p'){
			tamanho = 4;
		}
		else{
			tamanho = 1;
		}
		// Posicionar cada embarcação de acordo com a direção:
		if(direcao[0] == 'c'){
			for (int d=0;d<tamanho; d++){
				enderecos[(linha-d)][coluna] = conteudo;
			}
		}
		else if(direcao[0] == 'b'){
			for (int d=0;d<tamanho; d++){
				enderecos[(linha+d)][coluna] = conteudo;
			}
		}
		else if(direcao[0] == 'e'){
			for (int d=0;d<tamanho; d++){
				enderecos[linha][(coluna-d)] = conteudo;
			}
		}
		else if(direcao[0] == 'd'){
			for (int d=0;d<tamanho; d++){
				enderecos[linha][(coluna+d)] = conteudo;
			}
		}
		else{
			enderecos[linha][coluna] = conteudo;
		}
	}

	void mapa::set_endereco(int linha, int coluna, int conteudo){
		enderecos[linha][coluna] = conteudo;
	}

	int mapa::get_enderecos(int linha, int coluna){
		return enderecos[linha][coluna];
	}

	int mapa::checa_ataque(int linha, int coluna){
		int pos;
		pos = enderecos[linha][coluna];
		if(pos == 300){
			//Acertou na água;
			enderecos[linha][coluna] = 400;
			interface[linha][coluna] = "[~]";
			return 300;
		}
		else if(pos == 400){
			// Já atirou naquela posição
			cout << "Você já atirou nesse lugar, insira um novo ataque (linha/coluna):" << endl;
			return 400;
		}
		else{
			//Acertou alguma embarcação:

			return pos;
		}
	}

	int mapa::gera_aleatorio(){
		srand(time(NULL));
		int aleatorio = rand()%(2);
		return aleatorio;
	}