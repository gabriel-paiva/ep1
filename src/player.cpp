#include "../inc/player.hpp"

player::player(string nome, int quant_barcos){
	set_nome(nome);
	set_quant_barcos(quant_barcos);
}

player::~player(){}

void player::set_nome(string nome){
	this->nome = nome;
}

string player::get_nome(){
	return nome;
}

void player::set_quant_barcos(int quant_barcos){
	this->quant_barcos=quant_barcos;
}

int player::get_quant_barcos(){
	return quant_barcos;
}