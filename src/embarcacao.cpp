#include "../inc/embarcacao.hpp"

Embarcacao::Embarcacao(){
	set_nome(" ");
	set_vida(0);
	set_linha(0);
	set_coluna(0);
	set_direcao(" ");
}

Embarcacao::Embarcacao(string nome, int vida, int linha, int coluna, string direcao){
	set_nome(nome);
	set_vida(vida);
	set_direcao(direcao);
	set_linha(linha);
	set_coluna(coluna);
}

Embarcacao::~Embarcacao(){

}

void Embarcacao::set_nome(string nome){
	this->nome = nome;
}

string Embarcacao::get_nome(){
	return nome;
}

void Embarcacao::set_vida(int vida){
	this->vida = vida;
}

int Embarcacao::get_vida(){
	return vida;
}

void Embarcacao::set_linha(int linha){
	this->linha = linha;
}

int Embarcacao::get_linha(){
	return linha;
}

void Embarcacao::set_coluna(int coluna){
	this->coluna = coluna;
}

int Embarcacao::get_coluna(){
	return coluna;
}

void Embarcacao::set_direcao (string direcao){
	this->direcao = direcao;
}

string Embarcacao::get_direcao(){
	return direcao;
}

void Embarcacao::set_contador(int pos, int conteudo){
	contador[pos] = conteudo;
}

int Embarcacao::get_contador(int pos){
	return contador[pos];
}