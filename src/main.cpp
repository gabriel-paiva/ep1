#include <iostream>
#include <string>

#include "../inc/game.hpp"

using namespace std;

void menu (){
	int opcao;
	system("clear");
	cout << "-------------------------------------------" << endl;
	cout << "MAIN MENU: " << endl;
	cout << "1. Começar jogo" << endl;
	cout << "2. Sair do jogo" << endl;
	cout << "-------------------------------------------" << endl;
	cin >> opcao;
	switch(opcao){
		case 1:{
			game * jogo = new game();
			jogo->ingame();
			delete jogo;
			}break;
		case 2:
			exit(0);
			break;
		default:
			break;
	}
}

int novamente(){
	int opt;
	system ("clear");
	cout << "Deseja jogar novamente?" << endl;
	cout << "1. SIM"<< endl;
	cout << "2. NÃO"<< endl;
	cin >> opt;
	switch(opt){
		case 1:
			//Sim
			return 1;
			break;
		case 2:
			//Não
			return 0;
			break;
		default:
			break;

	}
	return 1;
}

int main(){
	int sair = 1;
	while(sair==1){
	menu();
	sair = novamente();
	}
    return 0;
}