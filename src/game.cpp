#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <chrono>

#include "../inc/game.hpp"

#include "../inc/embarcacao.hpp"
#include "../inc/mapa.hpp"
#include "../inc/player.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaavioes.hpp"

using namespace std;

	game::game(){}
	game::~game(){}

// Struct para guardar informações do arquivo txt
struct filebarcos{
		string nome, direcao;
		int linha, coluna;
	};
struct filebarcos fbarcos[24];

// Carrega o arquivo na struct
void game::carrega_struct(){
	int linha, coluna, i=0, mapa;
	string nome, direcao, slinha, scoluna, namefile;
	vector<string> copia;

	// Escolher qual será o arquivo lido
	system("clear");
	cout << "Escolha qual mapa será usado:" << endl;
	cout << "1. Mapa 1" << endl;
	cout << "2. Mapa 2" << endl;
	cout << "3. Mapa 3" << endl;
	cin >> mapa;
	switch(mapa){
		case 1:
			namefile = "./doc/map_1.txt";
			break;
		case 2:
			namefile = "./doc/map_2.txt";
			break;
		case 3:
			namefile = "./doc/map_3.txt";
			break;
		default:
			break;
	}

	copia = le_arquivo(namefile);
	// Separar a string em quatro variáveis diferentes:
	while(i<24){
	int primeiro = 0, espaco;
	espaco = copia[i].find(' ', primeiro);
	slinha = copia[i].substr(primeiro, (espaco-primeiro));
	primeiro = espaco+1;
	espaco = copia[i].find(' ', primeiro);
	scoluna = copia[i].substr((primeiro), (espaco-primeiro));	
	primeiro = espaco+1;
	espaco = copia[i].find(' ', primeiro);
	nome = copia[i].substr((primeiro), (espaco-primeiro));
	primeiro = espaco+1;
	espaco = copia[i].find(' ', primeiro);
	direcao = copia[i].substr((primeiro), (espaco-primeiro));
	// Transformar slinha e sconluna em inteiros
	linha = converte_str(slinha);
	coluna = converte_str(scoluna);
		fbarcos[i].nome = nome;
		fbarcos[i].direcao = direcao;
		fbarcos[i].linha = linha;
		fbarcos[i].coluna = coluna;
	i++;
	}
}

void game::ingame (){
	vector<Embarcacao *> lista_p1;
	vector<Embarcacao *> lista_p2;
	int aux_vida, atklinha, atkcoluna, aux_player;

	system("clear");
	string p1, p2;
	cout << "Digite o nome do primeiro jogador: " << endl;
	cin >> p1;
	system("clear");
	cout << "Digite o nome do segundo jogador: " << endl;
	cin >> p2;
	system("clear");

	// Criar os objetos pra cada player
	player * player1 = new player(p1, 12);
	player * player2 = new player(p2, 12);

	// Criar os objetos mapa_p1 e mapa_p2
	mapa * mapa_p1 = new mapa();
	mapa * mapa_p2 = new mapa();

	// Preencher os mapas com as informações do arquivo
	carrega_struct();

	// P1:
	for (int i=0; i<6;i++){
		lista_p1.push_back(new canoa(fbarcos[i].nome, fbarcos[i].linha,fbarcos[i].coluna));
		mapa_p1->set_enderecos(fbarcos[i].linha, fbarcos[i].coluna, i,fbarcos[i].nome, fbarcos[i].direcao);
	}
	for (int i=6; i<10;i++){
		lista_p1.push_back(new submarino(fbarcos[i].nome,fbarcos[i].linha,fbarcos[i].coluna,fbarcos[i].direcao));
		mapa_p1->set_enderecos(fbarcos[i].linha, fbarcos[i].coluna, i, fbarcos[i].nome, fbarcos[i].direcao);
	}
	for (int i=10; i<12;i++){
		lista_p1.push_back(new portaavioes(fbarcos[i].nome,fbarcos[i].linha,fbarcos[i].coluna,fbarcos[i].direcao));
		mapa_p1->set_enderecos(fbarcos[i].linha, fbarcos[i].coluna, i, fbarcos[i].nome, fbarcos[i].direcao);
	}
	// P2:
		for (int i=12; i<18;i++){
		lista_p2.push_back(new canoa(fbarcos[i].nome, fbarcos[i].linha,fbarcos[i].coluna));
		mapa_p2->set_enderecos(fbarcos[i].linha, fbarcos[i].coluna, (i-12), fbarcos[i].nome, fbarcos[i].direcao);
	}
	for (int i=18; i<22;i++){
		lista_p2.push_back(new submarino(fbarcos[i].nome,fbarcos[i].linha,fbarcos[i].coluna,fbarcos[i].direcao));
		mapa_p2->set_enderecos(fbarcos[i].linha, fbarcos[i].coluna, (i-12), fbarcos[i].nome, fbarcos[i].direcao);
	}
	for (int i=22; i<24;i++){
		lista_p2.push_back(new portaavioes(fbarcos[i].nome,fbarcos[i].linha,fbarcos[i].coluna,fbarcos[i].direcao));
		mapa_p2->set_enderecos(fbarcos[i].linha, fbarcos[i].coluna, (i-12), fbarcos[i].nome, fbarcos[i].direcao);
	}
//----------------------------------------------------------------------------------------------------------
	while(player1->get_quant_barcos()!=0 || player2->get_quant_barcos()!=0){
		// Jogada do p1:
		system("clear");
		mapa_p2->printa_interface();
		cout << player1->get_nome() << ", insira seu ataque (linha/coluna):" << endl;
		int posv = 400;
		while(posv==400){
			cin >> atklinha >> atkcoluna;
			if(atklinha == 13 || atkcoluna == 13){
				break;
			}
			posv = mapa_p2->checa_ataque(atklinha, atkcoluna);
		}
		// Botão de encerra o jogo
		if(atklinha == 13 || atkcoluna == 13){
			break;
		}
		if(posv == 300){
			cout << "Você acertou a água!" << endl;
		}
		else{
			// Checar a embarcação
			if (lista_p2[posv]->get_nome() == "porta-avioes"){
				//Gerar numero aleatório
				if(mapa_p2->gera_aleatorio()==1){
					//Acertou
						//Diminui a vida do barco
					aux_vida = lista_p2[posv]->get_vida();
					lista_p2[posv]->set_vida(aux_vida-1);
						//Faz as modificações necessárias nos mapas
					mapa_p2->set_endereco(atklinha, atkcoluna, 400);
					mapa_p2->set_interface(atklinha, atkcoluna, "[X]");
					//Verificar se o barco foi destruido pra diminuir a vida do player
					if(aux_vida-1==0){
						aux_player = player2->get_quant_barcos();
						player2->set_quant_barcos(aux_player-1);
						cout << "Você destruiu uma embarcação!" << endl;
					}
					else{
						cout << "Você acertou uma embarcação!"<< endl;
					}
				}
				else{
					//Desviou
					cout << "Seu ataque foi desviado!" << endl;
				}
			}
			else if (lista_p2[posv]->get_nome() == "submarino"){
				// Verificar qual a posição do submarino foi atingida
				if(lista_p2[posv]->get_linha() == atklinha && lista_p2[posv]->get_coluna() == atkcoluna){
					//Se for igual, quer dizer que é a posição 0 do submarino
					int aux_submarino;
					aux_submarino = lista_p2[posv]->get_contador(0);
					//Aumenta o contador da posicao 0:
					lista_p2[posv]->set_contador(0, (aux_submarino+1));
					// Se o contador for 1, dizer que acertou a embarcação
					if(aux_submarino+1 == 1){
						cout << "Você acertou uma embarcação, mas ela não foi tão afetada!"<< endl;
					} // Se for 2, destruir aquela posição da embarcação
					else if(aux_submarino+1 == 2){
						mapa_p2->set_endereco(atklinha, atkcoluna, 400);
						mapa_p2->set_interface(atklinha, atkcoluna, "[X]");
						//Diminui a vida do barco
						aux_vida = lista_p2[posv]->get_vida();
						lista_p2[posv]->set_vida(aux_vida-1);
						cout << "Você acertou uma embarcação!" << endl;
					}
				}
				else{
					// Posição 1 do submarino
					int aux_submarino;
					aux_submarino = lista_p2[posv]->get_contador(1);
					//Aumenta o contador da posicao 1:
					lista_p2[posv]->set_contador(1, (aux_submarino+1));
					// Se o contador for 1, dizer que acertou a embarcação
					if(aux_submarino+1 == 1){
						cout << "Você acertou uma embarcação, mas ela não foi tão afetada!"<< endl;
					} // Se for 2, destruir aquela posição da embarcação
					else if(aux_submarino+1 == 2){
						mapa_p2->set_endereco(atklinha, atkcoluna, 400);
						mapa_p2->set_interface(atklinha, atkcoluna, "[X]");
						//Diminui a vida do barco
						aux_vida = lista_p2[posv]->get_vida();
						lista_p2[posv]->set_vida(aux_vida-1);
						cout << "Você acertou uma embarcação!" << endl;
					}
				}
				//Verificar se o barco foi destruido pra diminuir a vida do player
					if(aux_vida-1==0){
						aux_player = player2->get_quant_barcos();
						player2->set_quant_barcos(aux_player-1);
						cout << "Você destruiu uma embarcação!" << endl;
					}
			}
			else{
				//É uma canoa
				//Destrói a canoa
				mapa_p2->set_endereco(atklinha, atkcoluna, 400);
				mapa_p2->set_interface(atklinha, atkcoluna, "[X]");
				//Diminui a vida do player
				aux_player = player2->get_quant_barcos();
				player2->set_quant_barcos(aux_player-1);
				cout << "Você destruiu uma embarcação!" << endl;

			}	
		}
		std::this_thread::sleep_for (std::chrono::seconds(1));
		system("clear");
		mapa_p2->printa_interface();
		std::this_thread::sleep_for (std::chrono::seconds(1));
		system("clear");

		// Checando o fim do jogo.
		if(player2->get_quant_barcos()==0){
			continue;
		}

		//Jogada do P2:
		mapa_p1->printa_interface();
		cout << player2->get_nome() << ", insira seu ataque (linha/coluna):" << endl;
		posv = 400;
		while(posv==400){
			cin >> atklinha >> atkcoluna;
			if(atklinha == 13 || atkcoluna == 13){
				break;
			}
			posv = mapa_p1->checa_ataque(atklinha, atkcoluna);
		}
		// Botão de encerra o jogo
		if(atklinha == 13 || atkcoluna == 13){
			break;
		}
		if(posv == 300){
			cout << "Você acertou a água!" << endl;
		}
		else{
			// Checar a embarcação
			if (lista_p1[posv]->get_nome() == "porta-avioes"){
				//Gerar numero aleatório
				if(mapa_p1->gera_aleatorio()==1){
					//Acertou
						//Diminui a vida do barco
					aux_vida = lista_p1[posv]->get_vida();
					lista_p1[posv]->set_vida(aux_vida-1);
						//Faz as modificações necessárias nos mapas
					mapa_p1->set_endereco(atklinha, atkcoluna, 400);
					mapa_p1->set_interface(atklinha, atkcoluna, "[X]");
					//Verificar se o barco foi destruido pra diminuir a vida do player
					if(aux_vida-1==0){
						aux_player = player1->get_quant_barcos();
						player1->set_quant_barcos(aux_player-1);
						cout << "Você destruiu uma embarcação!" << endl;
					}
					else{
						cout << "Você acertou uma embarcação!"<< endl;
					}
				}
				else{
					//Desviou
					cout << "Seu ataque foi desviado!" << endl;
				}
			}
			else if (lista_p1[posv]->get_nome() == "submarino"){
				// Verificar qual a posição do submarino foi atingida
				if(lista_p1[posv]->get_linha() == atklinha && lista_p1[posv]->get_coluna() == atkcoluna){
					//Se for igual, quer dizer que é a posição 0 do submarino
					int aux_submarino;
					aux_submarino = lista_p1[posv]->get_contador(0);
					//Aumenta o contador da posicao 0:
					lista_p1[posv]->set_contador(0, (aux_submarino+1));
					// Se o contador for 1, dizer que acertou a embarcação
					if(aux_submarino+1 == 1){
						cout << "Você acertou uma embarcação, mas ela não foi tão afetada!"<< endl;
					} // Se for 2, destruir aquela posição da embarcação
					else if(aux_submarino+1 == 2){
						mapa_p1->set_endereco(atklinha, atkcoluna, 400);
						mapa_p1->set_interface(atklinha, atkcoluna, "[X]");
						//Diminui a vida do barco
						aux_vida = lista_p1[posv]->get_vida();
						lista_p1[posv]->set_vida(aux_vida-1);
						cout << "Você acertou uma embarcação!" << endl;
					}
				}
				else{
					// Posição 1 do submarino
					int aux_submarino;
					aux_submarino = lista_p1[posv]->get_contador(1);
					//Aumenta o contador da posicao 1:
					lista_p1[posv]->set_contador(1, (aux_submarino+1));
					// Se o contador for 1, dizer que acertou a embarcação
					if(aux_submarino+1 == 1){
						cout << "Você acertou uma embarcação, mas ela não foi tão afetada!"<< endl;
					} // Se for 2, destruir aquela posição da embarcação
					else if(aux_submarino+1 == 2){
						mapa_p1->set_endereco(atklinha, atkcoluna, 400);
						mapa_p1->set_interface(atklinha, atkcoluna, "[X]");
						//Diminui a vida do barco
						aux_vida = lista_p1[posv]->get_vida();
						lista_p1[posv]->set_vida(aux_vida-1);
						cout << "Você acertou uma embarcação!" << endl;
					}
				}
				//Verificar se o barco foi destruido pra diminuir a vida do player
					if(aux_vida-1==0){
						aux_player = player1->get_quant_barcos();
						player1->set_quant_barcos(aux_player-1);
						cout << "Você destruiu uma embarcação!" << endl;
					}
			}
			else{
				//É uma canoa
				//Destrói a canoa
				mapa_p1->set_endereco(atklinha, atkcoluna, 400);
				mapa_p1->set_interface(atklinha, atkcoluna, "[X]");
				//Diminui a vida do player
				aux_player = player1->get_quant_barcos();
				player1->set_quant_barcos(aux_player-1);
				cout << "Você destruiu uma embarcação!" << endl;
			}
		}
		std::this_thread::sleep_for (std::chrono::seconds(1));
		system("clear");
		mapa_p1->printa_interface();
		std::this_thread::sleep_for (std::chrono::seconds(1));

		//Checando o fim do jogo
		if(player1->get_quant_barcos()==0){
			continue;
		}
	}//Fim do while que roda o jogo
//----------------------------------------------------------------------------------------------------------
system("clear");
int gameover;
cout << "GAME OVER" << endl;
if(player1->get_quant_barcos()==0){
	gameover = player2->get_quant_barcos();
	cout << player2->get_nome() << " venceu!" << endl;
	cout << player1->get_nome() << " destruiu " << 12-gameover << " embarcações." << endl;
	std::this_thread::sleep_for (std::chrono::seconds(4));
}
else if(player2->get_quant_barcos()==0){
	gameover = player1->get_quant_barcos();
	cout << player1->get_nome() << " venceu!" << endl;
	cout << player2->get_nome() << " destruiu " << 12-gameover << " embarcações." << endl;
	std::this_thread::sleep_for (std::chrono::seconds(4));
}

//----------------------------------------------------------------------------------------------------------
	// Apagando as embarcações do P1:
	for (Embarcacao *p : lista_p1){
		delete p;
	}
	lista_p1.clear();
	// Apagando as embarcações do P2:
	for (Embarcacao *p2 : lista_p2){
		delete p2;
	}
	lista_p2.clear();
	// Deletar os objetos mapa
	delete mapa_p1;
	delete mapa_p2;	
	// Deletar os objetos player
	delete player1;
	delete player2;
}

int game::converte_str(string str){
	int ans;
	if (str[1] == '0'){
		ans = 10;
	}
	else if (str[1] == '1'){
		ans = 11;
	}
	else if(str[1] == '2'){
		ans = 12;
	}
	else if(str[0] == '3'){
		ans = 3;
	}
	else if(str[0] == '4'){
		ans = 4;
	}
	else if(str[0] == '5'){
		ans = 5;
	}
	else if(str[0] == '6'){
		ans = 6;
	}
	else if(str[0] == '7'){
		ans = 7;
	}
	else if(str[0] == '8'){
		ans = 8;
	}
	else if(str[0] == '9'){
		ans = 9;
	}
	else if(str[0] == '0'){
		ans = 0;
	}
	else if(str[0] == '1'){
		ans = 1;
	}
	else if(str[0] == '2'){
		ans = 2;
	}

	return ans;
}

vector<string> game::le_arquivo(string namefile) {
	vector<string> lista_barcos;

	fstream leitor;
	leitor.open(namefile, fstream::in);

	//Checar erro
	if(leitor.fail()){
		cout << "Erro em abrir o arquivo" << endl;
	}
	else{
		//Ler arquivo
		while(!leitor.eof()){
			char flinha[256];
			string copia;
			// Pegar cada linha
			leitor.getline(flinha, 256);
			// Ignorar as linhas com #
			if(flinha[0] == '#' || flinha[0] == '\n'){
				leitor.ignore(256, '\n');
			}
			else{
				// Pegar as e separar em variaveis
				copia = flinha;
				// Evitar as linhas vazias que ficaram com lixo de memoria
				if(copia.size() >=14){
					lista_barcos.push_back(copia);
				}
			}
			
		}

	}

	leitor.close();

	return lista_barcos;
}
